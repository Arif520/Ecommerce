-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 19, 2018 at 01:02 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `new_shop`
--
CREATE DATABASE IF NOT EXISTS `new_shop` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `new_shop`;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `brand_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand_name`, `brand_description`, `publication_status`, `created_at`, `updated_at`) VALUES
(2, 'Mi Note 6', 'ncgncghnc', 1, '2018-07-06 12:54:06', '2018-07-06 14:31:25'),
(4, 'Mi Note 4X', 'Awsome Mobile', 1, '2018-07-06 14:30:40', '2018-07-06 14:31:09'),
(6, 'Tomy', 'A T-shirt (or t shirt, or tee) is a style of unisex fabric shirt named after the T shape of its body and sleeves', 1, '2018-07-18 00:52:16', '2018-07-18 00:52:49');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_description`, `publication_status`, `created_at`, `updated_at`) VALUES
(3, 'Sports', 'Bangladesh', 1, NULL, '2018-07-09 00:33:52'),
(9, 'Men', 'mhgmgh', 1, '2018-07-06 14:33:42', '2018-07-06 14:33:42'),
(10, 'Women', 'Woman\'s World is an American supermarket weekly magazine with a', 1, '2018-07-09 02:58:35', '2018-07-09 02:58:56'),
(11, 'Mobile', 'circulation of 1.6 million readers. Printed on paper generally associated', 1, '2018-07-09 02:59:26', '2018-07-09 02:59:26'),
(12, 'Electronics', 'with tabloid publications and priced accordingly, it concentrates on short', 1, '2018-07-09 03:00:10', '2018-07-09 03:00:10'),
(13, 'Shoes', 'articles about subjects such as weight loss, relationship advice and', 1, '2018-07-09 03:00:41', '2018-07-09 03:00:41'),
(14, 'Cloth', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam', 1, '2018-07-09 13:47:51', '2018-07-09 13:47:51');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `email_address`, `password`, `phone_number`, `address`, `created_at`, `updated_at`) VALUES
(1, 'MD.ARIFUR', 'RAHMAN', 'aparifurrahman@gmail.com', '$2y$10$pEEuZdfr09XMzxgfKM.B3OFtcZ6L/lz.g3kTigDipoUPIXyAH/Dom', '016733', 'lalkhan Bazar', '2018-07-21 08:17:52', '2018-07-21 08:17:52'),
(2, 'Arif', 'Rahman', 'apo@gfmail.com', '$2y$10$cgLtXMwYV7O9fljAnb.a7e6DBbl74MYQgRPx0Ompggwus7KaxNyem', '01787', 'lskdh ndh', '2018-08-18 23:46:27', '2018-08-18 23:46:27');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_03_191343_create_categories_table', 2),
(4, '2018_07_05_210132_create_brands_table', 3),
(5, '2018_07_05_224340_create_products_table', 4),
(6, '2018_07_20_194230_create_customers_table', 5),
(7, '2018_07_21_171326_create_shippings_table', 6),
(8, '2018_08_16_075318_create_orders_table', 7),
(9, '2018_08_16_075343_create_payments_table', 7),
(10, '2018_08_16_092906_create_order_details_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `order_total` double(10,2) NOT NULL,
  `order_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `shipping_id`, `order_total`, `order_status`, `created_at`, `updated_at`) VALUES
(1, 10, 6, 57000.00, 'pending', '2018-08-18 00:16:07', '2018-08-18 00:16:07'),
(2, 10, 6, 57000.00, 'pending', '2018-08-18 00:16:37', '2018-08-18 00:16:37'),
(3, 10, 6, 57000.00, 'pending', '2018-08-18 00:17:34', '2018-08-18 00:17:34'),
(4, 10, 6, 57000.00, 'pending', '2018-08-18 00:18:24', '2018-08-18 00:18:24'),
(5, 10, 6, 57000.00, 'pending', '2018-08-18 00:21:09', '2018-08-18 00:21:09'),
(6, 10, 6, 57000.00, 'pending', '2018-08-18 00:23:36', '2018-08-18 00:23:36'),
(7, 10, 6, 59700.00, 'pending', '2018-08-18 00:24:52', '2018-08-18 00:24:52'),
(8, 10, 6, 59700.00, 'pending', '2018-08-18 00:27:08', '2018-08-18 00:27:08'),
(9, 10, 6, 59700.00, 'pending', '2018-08-18 00:27:27', '2018-08-18 00:27:27'),
(10, 10, 6, 59700.00, 'pending', '2018-08-18 00:36:31', '2018-08-18 00:36:31'),
(11, 10, 6, 59700.00, 'pending', '2018-08-18 00:38:45', '2018-08-18 00:38:45'),
(12, 10, 6, 59700.00, 'pending', '2018-08-18 00:44:12', '2018-08-18 00:44:12'),
(13, 10, 6, 59700.00, 'pending', '2018-08-18 00:50:33', '2018-08-18 00:50:33'),
(14, 10, 6, 59700.00, 'pending', '2018-08-18 00:51:17', '2018-08-18 00:51:17'),
(15, 10, 6, 59700.00, 'pending', '2018-08-18 02:24:36', '2018-08-18 02:24:36'),
(16, 10, 6, 9000.00, 'pending', '2018-08-18 03:26:05', '2018-08-18 03:26:05'),
(17, 10, 6, 0.00, 'pending', '2018-08-18 03:43:42', '2018-08-18 03:43:42'),
(18, 2, 7, 12000.00, 'pending', '2018-08-18 23:46:40', '2018-08-18 23:46:40'),
(19, 2, 7, 7200.00, 'pending', '2018-08-18 23:59:53', '2018-08-18 23:59:53'),
(20, 2, 7, 4500.00, 'pending', '2018-08-19 02:52:08', '2018-08-19 02:52:08');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` double(10,2) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `product_id`, `product_name`, `product_price`, `product_quantity`, `created_at`, `updated_at`) VALUES
(1, 12, 10, 'J3', 9000.00, 1, '2018-08-18 00:44:13', '2018-08-18 00:44:13'),
(2, 12, 11, 'Nokia 2', 12000.00, 4, '2018-08-18 00:44:13', '2018-08-18 00:44:13'),
(3, 12, 12, 'T-shart', 900.00, 3, '2018-08-18 00:44:13', '2018-08-18 00:44:13'),
(4, 16, 10, 'J3', 9000.00, 1, '2018-08-18 03:26:06', '2018-08-18 03:26:06'),
(5, 18, 11, 'Nokia 2', 12000.00, 1, '2018-08-18 23:46:40', '2018-08-18 23:46:40'),
(6, 19, 9, 'J7', 1800.00, 4, '2018-08-18 23:59:53', '2018-08-18 23:59:53'),
(7, 20, 12, 'T-shart', 900.00, 5, '2018-08-19 02:52:08', '2018-08-19 02:52:08');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `order_id`, `payment_type`, `payment_status`, `created_at`, `updated_at`) VALUES
(1, 2, 'Cash', 'pending', '2018-08-18 00:16:37', '2018-08-18 00:16:37'),
(2, 3, 'Cash', 'pending', '2018-08-18 00:17:35', '2018-08-18 00:17:35'),
(3, 4, 'Cash', 'pending', '2018-08-18 00:18:24', '2018-08-18 00:18:24'),
(4, 5, 'Cash', 'pending', '2018-08-18 00:21:09', '2018-08-18 00:21:09'),
(5, 6, 'Cash', 'pending', '2018-08-18 00:23:36', '2018-08-18 00:23:36'),
(6, 9, 'Cash', 'pending', '2018-08-18 00:27:27', '2018-08-18 00:27:27'),
(7, 10, 'Cash', 'pending', '2018-08-18 00:36:31', '2018-08-18 00:36:31'),
(8, 11, 'Cash', 'pending', '2018-08-18 00:38:46', '2018-08-18 00:38:46'),
(9, 12, 'Cash', 'pending', '2018-08-18 00:44:12', '2018-08-18 00:44:12'),
(10, 13, 'Cash', 'pending', '2018-08-18 00:50:33', '2018-08-18 00:50:33'),
(11, 14, 'Cash', 'pending', '2018-08-18 00:51:17', '2018-08-18 00:51:17'),
(12, 15, 'Cash', 'pending', '2018-08-18 02:24:36', '2018-08-18 02:24:36'),
(13, 16, 'Cash', 'pending', '2018-08-18 03:26:06', '2018-08-18 03:26:06'),
(14, 17, 'Cash', 'pending', '2018-08-18 03:43:42', '2018-08-18 03:43:42'),
(15, 18, 'Cash', 'pending', '2018-08-18 23:46:40', '2018-08-18 23:46:40'),
(16, 19, 'Cash', 'pending', '2018-08-18 23:59:53', '2018-08-18 23:59:53'),
(17, 20, 'Cash', 'pending', '2018-08-19 02:52:08', '2018-08-19 02:52:08');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` double(10,2) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `long_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `publication_status` tinyint(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `brand_id`, `product_name`, `product_price`, `product_quantity`, `short_description`, `long_description`, `product_image`, `created_at`, `updated_at`, `publication_status`) VALUES
(7, 12, 6, 'Nokia 6', 16000.00, 6, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam&nbsp;&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam&nbsp;</p>', 'product-images/Nokia 6.jpg', '2018-07-09 13:39:54', '2018-07-09 13:39:54', 1),
(9, 12, 5, 'J7', 1800.00, 1, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam&nbsp;</p>', 'product-images/J7.jpg', '2018-07-09 13:44:21', '2018-07-09 13:44:21', 1),
(10, 12, 5, 'J3', 9000.00, 1, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam&nbsp;</p>', 'product-images/J2.jpg', '2018-07-09 13:45:12', '2018-07-16 02:33:37', 1),
(11, 12, 6, 'Nokia 2', 12000.00, 1, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam&nbsp;</p>', 'product-images/Nokia 2.png', '2018-07-09 13:46:03', '2018-07-09 13:46:03', 1),
(12, 9, 6, 'T-shart', 900.00, 4, 'A T-shirt (or t shirt, or tee) is a style of unisex fabric shirt named after the T shape of its body and sleeves', '<p>A&nbsp;<strong>T-shirt</strong>&nbsp;(or&nbsp;<strong>t shirt</strong>, or&nbsp;<strong>tee</strong>) is a style of&nbsp;<a href=\"https://en.wikipedia.org/wiki/Unisex\">unisex</a>&nbsp;fabric&nbsp;<a href=\"https://en.wikipedia.org/wiki/Shirt\">shirt</a>&nbsp;named after the T shape of its body and sleeves.&nbsp;&nbsp;A&nbsp;<strong>T-shirt</strong>&nbsp;(or&nbsp;<strong>t shirt</strong>, or&nbsp;<strong>tee</strong>) is a style of&nbsp;<a href=\"https://en.wikipedia.org/wiki/Unisex\">unisex</a>&nbsp;fabric&nbsp;<a href=\"https://en.wikipedia.org/wiki/Shirt\">shirt</a>&nbsp;named after the T shape of its body and sleeves</p>', 'product-images/T-shart.jpg', '2018-07-18 00:53:50', '2018-07-18 00:55:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `shippings`
--

CREATE TABLE `shippings` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shippings`
--

INSERT INTO `shippings` (`id`, `full_name`, `email_address`, `phone_number`, `address`, `created_at`, `updated_at`) VALUES
(1, 'MD.ARIFURRAHMAN', 'aparifurrahman@gmail.com', '016733', 'gdgdgt', '2018-07-21 13:08:28', '2018-07-21 13:08:28'),
(2, 'MD.ARIFURRAHMAN', 'aparifurrahman@gmail.com', '016733', 'gdgdgt', '2018-07-21 13:08:35', '2018-07-21 13:08:35'),
(3, 'ArifRahman', 'aparifurrahman@gmail.com', '0167', 'lalkhan bazar', '2018-08-16 01:02:24', '2018-08-16 01:02:24'),
(4, 'ArifRahman', 'aparifurrahman@gmail.com', '0167', 'yhrtdyuhdrh', '2018-08-16 05:04:08', '2018-08-16 05:04:08'),
(5, 'ArifRahman', 'aparifurrahman@gmail.com', '0167', 'gfhfg', '2018-08-17 22:52:13', '2018-08-17 22:52:13'),
(6, 'ArifRahman', 'aparifurrahman@gmail.com', '0167', 'gfhfg', '2018-08-18 00:15:20', '2018-08-18 00:15:20'),
(7, 'ArifRahman', 'apo@gfmail.com', '01787', 'lskdh ndh', '2018-08-18 23:46:37', '2018-08-18 23:46:37');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Arifur Rahman', 'aparif0@gmail.com', '$2y$10$IIkbVPOHFul0EDahQmXaWOG6zh77EG9LMphdE68.TlXH5IPTkJPHu', 'JwmJuBsYlSj0Obrr9xvrRTGHGCJOBHFQnszjj5vTxYsph0NMBTYzbI3SzZpe', '2018-06-28 01:36:44', '2018-06-28 01:36:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shippings`
--
ALTER TABLE `shippings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `shippings`
--
ALTER TABLE `shippings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
