@extends('front-end.master')

@section('body')

    <!--content-->
            <div class="content">

                <div class="row">
                    <div class="col-md-12 well text-center text-success">
                        <br>
                        Dear {{ Session::get('customerName') }} . You have to give us product shipping info to complete your valuable order. If your billing info are same then just press on save shipping info button.
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-md-offset-2">
                        <br>
                        <h3 class="text-center text-success">Shipping Information Goes Here.....</h3>
                        <br>
                        {{ Form::open(['route'=>'new-shipping','method'=>'POST']) }}
                        <div class="form-group">
                            <input type="text" value="{{ $customer->first_name.''.$customer->last_name }}" name="full_name" class="form-control" placeholder="Full Name">
                        </div>
                        <div class="form-group">
                            <input type="email" value="{{ $customer->email_address }}" name="email_address" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="text" value="{{ $customer->phone_number }}" name="phone_number" class="form-control" placeholder="Phone Number">
                        </div>
                        <div class="form-group">
                            <textarea name="address" class="form-control" placeholder="Address">{{ $customer->address }}</textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="btn" class="form-control btn btn-info" value="Save Shipping Info">
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        <!--new-arrivals--
    <!--content-->
@endsection