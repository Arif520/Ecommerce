@extends('front-end.master')

@section('body')

    <!--content-->
    <div class="content">

        <div class="row">
            <div class="col-md-12 well text-center text-success">
                <br>
                Dear {{ Session::get('customerName') }} . You have to give us product payment method...
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-2 well">
                 <h1>Payment Form......</h1>
                {{ Form::open(['route'=>'new-order','method'=>'POST']) }}
                <table class="table table-bordered">
                        <tr>
                            <th>Cash on Delivery</th>
                            <td><input type="radio" name="payment_type" value="Cash">Cash On Delivery</td>
                        </tr>
                        <tr>
                            <th>Paypal</th>
                            <td><input type="radio" name="payment_type" value="Paypal">Paypal</td>
                        </tr>
                        <tr>
                            <th>BKash</th>
                            <td><input type="radio" name="payment_type" value="Bkash">BKash</td>
                        </tr>
                        <tr>
                            <th></th>
                            <td><input type="submit" name="btn" value="Confirm Order" class="form-control btn btn-info"></td>
                        </tr>
                    </table>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--new-arrivals--
<!--content-->
@endsection